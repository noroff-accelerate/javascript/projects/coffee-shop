export function formatToCurrency(amount) {
    return `${amount.toFixed(2)} NOK`
}