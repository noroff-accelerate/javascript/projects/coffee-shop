import { formatToCurrency } from "./formatter.js";

const coffeeTypeElement = document.getElementById("coffeeType");
const addItemElement = document.getElementById("btnAddItem");
const invoiceItemsElement = document.getElementById("invoiceItems");
const totalElement = document.getElementById("total");
const payElement = document.getElementById("pay");
const walletElement = document.getElementById("wallet");
const inventoryElement = document.getElementById("inventory");

addItemElement.addEventListener("click", addInvoiceItem);
payElement.addEventListener("click", handlePayClicked);

const baseCoffeeApiUrl = "https://puzzled-seasoned-ornament.glitch.me/coffee"

let invoiceItems = [];
let total = 0;
let walletAmount = 100;
let coffeeDetails = [];
//async version call
// const coffeeDetails = await fetchCoffeeDetails();
//populateCoffeeTypes(coffeeDetails);

//async version
// async function fetchCoffeeDetails() {
//     try {
//         const response = await fetch("baseCoffeeApiUrl");
//         return await response.json();
//     } catch (error) {
//         console.log(error);
//     }
// }


//fetch version
fetch(baseCoffeeApiUrl)
.then(x => x.json())
.then(x => coffeeDetails = x)
.then(x => populateCoffeeTypes(x))
.catch(error => console.log(error));

function populateCoffeeTypes(coffeeDetails) {
    for (let i = 0; i < coffeeDetails.length; i++) {
        const coffeeDetail = coffeeDetails[i];

        const coffeeOption = document.createElement("option");

        coffeeOption.text = `${coffeeDetail.description} - ${formatToCurrency(coffeeDetail.price)}`;
        coffeeOption.value = coffeeDetail.id;

        coffeeTypeElement.append(coffeeOption);
    }
}

function displayInvoiceItem(item) {
    const invoiceItemElement = document.createElement("li");
    invoiceItemElement.textContent = `${item.detail.description} * ${item.quantity}`;
    invoiceItemsElement.append(invoiceItemElement);
}

function addInvoiceItem() {
    const selectedCoffee = coffeeDetails.find(x => x.id === Number(coffeeTypeElement.value));

    const newItem = {
        detail: selectedCoffee,
        quantity: Number(quantity.value)
    }

    invoiceItems.push(newItem);
    const itemTotal = selectedCoffee.price * newItem.quantity
    total += itemTotal;

    displayInvoiceItem(newItem);
    updateTotalElement();
}

function updateTotalElement() {
    totalElement.textContent = `Total: ${formatToCurrency(total)}`
}

function updateWalletElement() {
    walletElement.textContent = `Wallet: ${formatToCurrency(walletAmount)}`;
}

function handlePayClicked() {
    const input = prompt(`Please enter an amount (remaining total is ${formatToCurrency(total)})`)
    const payAmount = Number(input);

    if (isNaN(payAmount) || payAmount < 0) {
        alert(`Input value ${input} is not a valid number, please enter a valid number`);
        return;
    }

    if (walletAmount < payAmount) {
        alert(`Wallet amount ${formatToCurrency(walletAmount)} is less than the total entered`);
        return;
    }

    walletAmount -= payAmount;

    total -= payAmount;
    walletAmount += total < 0 ? Math.abs(total) : 0;
    total = total < 0 ? 0 : total;
    updateTotalElement();   
    updateWalletElement(); 
}