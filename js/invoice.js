let total = 0;

function setTotal(newTotal) {
    total = newTotal;
}

function getTotal() {
    return total;
}

const invoice = {
    getTotal,
    setTotal,
};

export default invoice;