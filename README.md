# Coffee Shop

Coffee shop is a simple web application written in plain JavaScript which simulates an online coffee ordering system.

## Installation

Run `index.html` using an HTTP server plugin, such as LiveServer. 

## Contributing

Pull requests are not accepted.

## License

Copyright 2023, Noroff Accelerate AS